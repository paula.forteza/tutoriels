# Comment envoyer des courriels en masse

## Rechercher des solutions

Commencer par rechercher les aternatives à MailChimp, en allant sur le site [AlternativeTo.net](http://alternativeto.net/) et en tapant `milchimp` dans la barre de recherche.

Affiner la recherche en précisant "Only Open Source" pour rechercher uniquement des solutions libres, que nous puissions auto-héberger (nous voulons garder la maîtrise de nos données). Une liste s'affiche :
    http://alternativeto.net/software/mailchimp/?license=opensource

### Évaluer Mailtrain

Le premier logiciel proposé est [MailTrain](http://alternativeto.net/software/mailtrain/]. Cliquer sur le [site officiel](https://mailtrain.org/). Le site est propre et s'adresse à des techniciens comme moi. Tiens c'est développé en NodeJS, pourquoi pas.

Chercher le lien vers le code source. Il y a tant de services dits _open source_ dont le lien vers le code source est difficilé voir impossible à trouver... Mais ici tout va bien, un bouton, en évidence en haut de la page, indique [Source on GitHub](https://github.com/Mailtrain-org/mailtrain).

Tout de suite, aller sur ce dépôt, vérifier le nombre d'étoiles (2209),  le nombre de _commits_ (553), le nombre de contributeurs (25) et la licence (GPL-3.0). Vérifier aussi que ce projet est bien vivant en regardant la [fréquence des derniers _commits_](https://github.com/Mailtrain-org/mailtrain/commits/master) (plusieurs commits par mois). Il y a aussi une [documentation d'installation](https://github.com/Mailtrain-org/mailtrain#installation). Un fichier [CHANGELOG](https://github.com/Mailtrain-org/mailtrain/blob/master/CHANGELOG.md) montre que des versions stables sont réalisées régulièrement. Tous les indicateurs sont donc au vert, il s'agit d'un projet libre, dynamique, avec une communauté.

Par acquis de conscience, vérifier le fichier [LICENSE](https://github.com/Mailtrain-org/mailtrain/blob/master/LICENSE). Il s'agit bien d'une version non modifiée de la licence libre [licence publique générale GNU](https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU).

### Évaluer Sympa

Le second logiciel proposé par AlternativeTo est [Sympa](http://alternativeto.net/software/sympa/). C'est un bon logiciel (que j'ai utilisé il y a bien longtemps), mais plus adapté aux listes de diffusions qu'à l'envoi en masse de courriels. Passer au suivant...

### Évaluer phpList

Le troisième logiciel s'appelle [phpList](http://alternativeto.net/software/phplist/). Tiens un logiciel qui commence par _php_, cela me ramène une vingtaine d'années en arrière. Les copies d'écran ont elles aussi un léger air désuét.

Aller sur le [site officiel](https://phplist.com/). C'est un site à la _startup_ qui propose de vendre un service. Le titre de la page parle d'_open source_. Pourtant pas de lien visible vers le code source. Peut-être que le lien [_Community_](https://www.phplist.org/) dans le pied de page va m'y conduire... Cliquer dessus : c'est clairement le pendant communautaire du site commercial précédent. Pourtant, pas de lien en évidence vers un dépôt de code source. Une icône de téléchargement en haut de la page propose de télécharger les dernières versions officielles, mais pas de trace d'un dépot Git. Le développement serait-il fermé avec seulement des versions officielles publiées et sans contribution extérieure ?

Regarder du côté du site de téléchargement :tiens c'est hébergé chez [Sourceforge](https://sourceforge.net/projects/phplist/files/) ce qui est de plus en plus rare, la nostalgie continue... Cliquer sur l'onglet [code](https://sourceforge.net/p/phplist/code/HEAD/tree/) ; tomber sur un dépôt Subversionc(un ancêtre de Git) dont le [dernier commit](https://sourceforge.net/p/phplist/code/commit_browser) date de 2014. C'est trop vieux ; cela ne colle pas avec les dates des dernières versions officielles de phpList qui remontent à 2017...

Essayer autrement. Rechercher "phplist source code" sur Internet. La première réponse est la bonne : un [dépôt GitHub](https://github.com/phpList/phplist3).

Le nombre d'étoiles est correct (112), le nombre de _commits_ (2535) et de contributeurs (37) aussi, la licence est bien libre (AGPL-3.0). Les derniers _commits_ sont récents.

Zut, en continuant à chercher, s'apercevoir que la version 3 actuelle va être remplacée par une version 4, qui a son [propre dépôt Git](https://github.com/phpList/phplist4-core) (sans doute une réécriture complète ?) dont le développement a commencé depuis 2013, [mais qui stagne depuis](https://github.com/phpList/phplist4-core/graphs/contributors).

Est-on dans le cas d'un logiciel dont la version actuelle montre ses limites, mais dont la version suivante n'arrivera jamais ?

Cela commence à faire beaucoup de points négatifs. Tans pis pour phpList.

### Évaluer le logiciel utilisé par les Insoumis

Un ancien collègue, [Guillaume Royer](https://github.com/guilro) avait parlé de la solution qu'il développait pour la France insoumise. Rechercher dans [ses dépôts](https://github.com/guilro?tab=repositories).

Ah il s'agit d'un [fork de Mailtrain](https://github.com/guilro?tab=repositories) !

Si cela a marché pour la campagne présidentielle JLM2017, cela fera sûrement l'affaire pour Paula et sa circonscription des Français d'Amérique latine et des Caraïbes. **Va pour Mailtrain**.

### Retour aux sources

Un [contributeur extérieur](https://framagit.org/nvallas) me propose une solution en ligne de commande :

```bash
#!/bin/bash

for i in `cat $1`
do
	#on cherche l'adresse dans la liste des courriels déjà envoyés
	grep $i fait.txt > /dev/null
	#si l'adresse est présente, on passe à la suite
	if [ $? -eq 0 ];
	then
		echo $i "Le message a déjà été envoyé"
	#si l'adresse n'est pas présente le message n'a pas encore été envoyé
	else
		echo "Envoi du message vers : " $adresse
		echo "Bonjour... blablabla..."  > message.txt
		#si la localisation est en UTF8 on converti en latin1
		iconv -f utf8 -t latin1 message.txt   > message2.txt
		cat message2.txt|mail -r "adresseDeReponse@mondomaine.fr" -s "sujet du message" $i > /dev/null 2>&1
		#On attends 2s pou ne pas surcharger le serveur (60s/2s=30 messages/ min; 1800/heure)
		sleep  2
		echo $i >> fait.txt
	fi
done
```

C'est simple, c'est beau... mais cela ne gère pas quelques _détails_ comme la gestion du désabonnement, l'édition des courriels en [WYSIWYG](), etc. Dommage ! Et un grand merci !

### Spammer pour le bien

19 septembre, un nouvel arrivant sort en version beta et apparaît sur [Hacker News](https://news.ycombinator.com/item?id=15265278). Son nom est tout un programme [mail-for-good](https://github.com/freeCodeCamp/mail-for-good).

Regarder si avant de faire le bien en envoyant massivement des courriels, il permet déjà de faire bien des envois massifs de courriels...

Une [video montre ses fonctionnalités](https://www.youtube.com/watch?v=_7U03GVD4a8) : prometteur, mais peut-être encore un peu sommaire.

Pas de souci côté licence : une [licence libre BSD](https://github.com/freeCodeCamp/mail-for-good/blob/master/LICENSE.md). [19 contributeurs](https://github.com/freeCodeCamp/mail-for-good/graphs/contributors) mais 2 seulement semblent actifs. [Pas beaucoup de contributions](https://github.com/freeCodeCamp/mail-for-good/graphs/commit-activity) ces dernières semaines, bien que le produit soit en beta : étonnant, généralement quand un produit sort en beta, beaucoup de correctifs pleuvent. Par contre beaucoup d'étoiles GitHub, plus que _Mailtrain_, peut-être un effet _Hacker News_ ?

À garder en réserve si _Mailtrain_ ne convient pas finalement.

