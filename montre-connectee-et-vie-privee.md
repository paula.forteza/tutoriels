# Une montre connectée respectueuse de la vie privée

Il existe 3 prinipaux systèmes d'exploitation pour les montres connectées : WatchOS (Apple), Android (Google) & Tizen (Samsung).

Aucun n'est totalement libre, même si Android est basé sur importante une couche _open source_ par dessus le noyau Linux et si Tizen est un système d'exploitation très proche d'un PC sous GNU/linux. Malheureusement, à chaque fois, des logiciels cruciaux de la montre sont propriétaires.

Heureusement, une initiative (menée par un français, Florent Revest) cherche à développer un systèeme d'exploitation libre pour les montres : [AsteroidOS](https://asteroidos.org/). Aujourd'hui (8 septembre 2017), ce projet est encore en version Alpha 1.0 et ne fonctionne que sur quelques montres Android, mais il semble très bien parti.

Et c'est là qu'intervient un toulousain Arunan Sathasivan, qui vient de lancer le [financement participatif](https://fr.ulule.com/connect-watch/) d'une montre conçue pour AsteroidOS : la [Connect Watch](http://connect-watch.com/).

En apprenant aujourd'hui la nouvelle sur le [Journal du hacker](https://www.journalduhacker.net/), je me suis empressé de pré-commander une montre à 99 €. Le site annonce qu'elle sera livrée à l'Assemblée la dernière semaine d'Octobre. À suivre...

J'ai hâte d'avoir à mon poignet une montre en laquelle je puisse faire confiance pour ne pas envoyer malgré moi mes données personnelles (localisation, rythme cardiaque, activité sportive, photographies, son, etc) un peu partout sur Internet. Tellement hâte que j'ai préféré choisir la version qui ne fait pas téléphone car elle sera livrée un mois plus tôt.
